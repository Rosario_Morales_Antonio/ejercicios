public class Principal{

    //metodo estatico principal
    public static void main (String []args){
        //invoca al metodo abstracto engine
        //colocar de manera explicita el int o long
       System.out.println("==> CalculadoraInt ---> resultado ="+engine((int)6, (int)5).calculate ( 0,  0));
       System.out.println("==> CalculadoraLong ---> resultado ="+engine((long)8,(long)2).calcular(  0,  0));
    }
private static  CalculadoraInt engine(int a, int b) {
//retorna operacion lambda
//retorno de funcionalidad
    //implementacion del metodo abstracto engine 
    return (  x, y)-> a*b;
}
private static  CalculadoraLong engine(long a, long b){
    return (  x, y)-> a-b;
    /*
    Scope
    X e Y son variables locales
    engine(long x, long y ) Error por que ya existen en el programa
    private static  CalculadoraLong engine(long x, long y){
    return (  a, b)-> a-b;
    */
    //variables de manera Global o Local? Siempr sabr
    //
}
}